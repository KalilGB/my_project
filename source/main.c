#include <stdio.h>
#include "dice.h"

int main(){
	
	int a;	
	
	printf("Digite o numero de faces que o nosso dado tera: ");
	scanf("%d", &a);

	initializeSeed();
	printf("Let's roll the dice: %d\n", rollDice(a));
	return 0;
}
